
import scipy.stats as stats

def pdf(x):
    """Mystery PDF."""
    return 0.5*stats.norm(0, 1).pdf(x) + 0.5*stats.norm(2, 0.5).pdf(x)