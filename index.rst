.. BIOS821 documentation master file, created by
   sphinx-quickstart on Thu Nov  2 11:04:51 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

BIOS821: Data Science Tools
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   scratch/UnixShell01.ipynb
   scratch/UnixShell02.ipynb
   scratch/UnixShell03.ipynb
   scratch/UnixShell04.ipynb
   scratch/Python01.ipynb
   scratch/Python02.ipynb
   scratch/Python03.ipynb
   scratch/Python04A.ipynb
   scratch/Python04B.ipynb
   scratch/Python05.ipynb
   scratch/Python06.ipynb
   scratch/Python06A.ipynb
   scratch/Python06B.ipynb
   scratch/Python06C.ipynb
   scratch/Python07A.ipynb
   scratch/Python07B.ipynb
   scratch/Python08A.ipynb
   scratch/Python09A.ipynb
   scratch/Python09B.ipynb
   scratch/Python10.ipynb
   scratch/PythonIsNotR.ipynb
   scratch/UsingDatabases01.ipynb
   scratch/UsingDatabases02.ipynb
   scratch/UsingDatabases03.ipynb
   scratch/UsingDatabases04.ipynb
   scratch/Using_Git_and_GitHub.ipynb

Quizzes
==================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   quiz/EntryQuiz.ipynb
   quiz/01_UnixBasics.ipynb
   quiz/02_UnixTextAndArithmetic.ipynb
   quiz/03_UnixShellScripts.ipynb
   quiz/100+ "Challenging" Python Exercises.ipynb
   scratch/Python9A_Exercise.ipynb
   scratch/Python9B_Exercise.ipynb

Homework
====================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   homework/HW01_Unix_Shell_1.ipynb
   homework/HW02_Unix_Shell_2.ipynb
   homework/HW_Caesar.ipynb
   homework/HW_ORFs.ipynb
   homework/HW_KMER.ipynb
   homework/HW_Wolfram.ipynb
   homework/HW_NumericalAlgorithms.ipynb
   homework/HW_Data_Munging.ipynb

Midterm exam
======================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   exams/Midterm.ipynb

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
