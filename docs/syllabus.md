# DS01: Software tools for data science

A data scientist needs to master several different tools to obtain, process, analyze, visualize and interpret large biomedical data sets such as electronic health records, medical images, and genomic sequences. It is also critical that the data scientist masters the best practices associated with using these tools, so that the results are robust and reproducible. Foundational tools for students to assemble a data science toolkit that will be taught are the Unix shell, text editors, regular expressions, relational and NoSQL databases, and the Python programming language for data munging, visualization and machine learning.

**1**. Literate programming in Jupyter

- Introducing Jupyter
- Using the text editor
- Using the terminal
- The Jupyter notebook
- Literate programming
- Elements of Markdown
- Polyglot programming

**2**. Unix shell I

- File and directory management
- Viewing text
- Using `man` and `apropos`
- Streams and pipes
- Unix tools for text manipulation
- Using `wget`, `tar` and `md5sum`
- Finding files
- Finding text in files

**3**. Unix shell II

- Using `vi` and `emacs`
- Loops
- Branching
- Variables
- Writing and executing shell scripts
- `diff` and `patch`
- `make` and `cmake`

**4**. Using version control 1

- Version control and `git` concepts
- Creating a repository
- Using `git config`
- The `.gitignore` file
- Add file(s)
- Commit file(s)
- Ignoring files and directories
- Working with remote repositories
- Using `clone`, `push` and `pull`
- Undoing `add`
- Undoing `commit`
- Fixing log messages

**5**. Using version control 2

- Branching
- Merging
- Fixing merge errors
- Creating and merging pull requests
- Visualizing branches
- Using `git diff` and `git diff-tree`
- Tagging
- More on `git log`
- Branch and merge
- Preparing a patch
- Rebasing a branch
- Working with large files

**6**. Regular expressions

- Matching characters
- Alternatives
- Quantifiers
- Meta-characters
- Character sets
- Capture groups
- Using regex on the command line
- Using regex in an editor

**7**. Introduction to Python

- Read-eval-print-loop REPL
- Interactive calculator
- Basic types
- Basic collections
- Operators
- Variable assignment
- Control flow - `if-elif-else` and the ternary operator
- Looping with `while` and `for`
- Useful built-in functions
- Writing a custom function
- The standard library
- Importing packages
- Virtual environments

**8**. Data collections

- Strings as collections
- Tuples, named tuples and tuple unpacking
- Lists, stacks, queues, deques
- Sets
- Dictionaries, `defaultdict` and counters
- Data structures and big-O complexity of operations

**9**. Functions

- Positional and named arguments
- Default arguments
- `args`, `kwargs` and keyword-only arguments
- Care with mutable arguments
- Function `docstring`
- Recursive functions
- Using Python modules with the `import` statement
- Creating Python modules

**10**. Loops, iterators, generators and comprehensions

- Comprehensions
- Iterators and iterables
- Generators and lazy evaluation
- Asynchronous programming

**11**. Object-oriented programming

- Classes combine data and behavior
- Creating a class
- Special methods
- More on `self`
- Inheritance
- Abstract base classes and the `abc` package
- Composition
- Design patterns in Python

**12**. Functional programming

- Pure and impure functions
- Lambda functions
- Higher order functions
- Function composition and chaining
- Currying and partial application
- Decorators
- Context managers
- Using `functools`, `itertools` and `operator` modules
- Using `toolz` for functional programming

**13**. Working with text I

- String methods
- String methods
- Regular expressions with `re`
- Custom string formatting
- ASCII and Unicode

**14**. Working with text II

- Using `configParser`
- Using `argparse`
- Using `biopython`
- Natural language processing with `spaCy`
- Topic modeling with with `gensim`

**15**. Working with numbers I

- Vectors, matrices and arrays with `numpy`
- Indexing
- Vectorization and `ufuncs`
- Broadcasting
- Reductions and the `axis` argument
- Random numbers and permutations with `numpy.random`
- Linear algebra with `scipy.linalg`
- Least squares
- Numerical optimization and Newton's method
- Numerical integration and adaptive quadrature

**16**. Working with numbers II

- Integration with `scipy.integrate`
- Optimization with `scipy.optimize`
- Interpolation with `scipy.interpolate`
- Statistics with `scipy.stats`

**17**. Working with dates and times

- Using `timeit`
- Using `time`
- Using `datetime`
- Using `arrow`

**18**. Data munging I

- Series, DataFrame and Panel in `pandas`
- Obtaining data
- Indexing
- Selecting columns
- Selecting rows
- Working with missing data
- Statistical methods
- Special methods `category` and `datetime` types
- Using `pipe` to chain methods

**19**. Data munging II

- Making data tidy
- Special string methods
- Reshaping and pivot tables
- Combining data frames
- Split-apply-combine with `groupby`, `agg`, `transform`, `filter` and `apply`
- Working with time series data
- Plotting with `pandas`

**20**. Working with relational databases

- Using SQL with `pandas`
- Working with `sqlite3`
- Using `sqlalchemy`

**21**. Working with other databases

- Key-value: `redis`
- Columnar store: `feather`
- Document store: `MongoDB`
- Hierarchical store: `HDF5`

**22**. Data visualization

- Using `matplotlib`
- Using `seaborn`
- Using `ggplot2`

**23**. Reactive data visualziaiotn

- Jupyter widgets
- Using `plotly.dash`
- Layout
- Interactivity
- Live updates

**24**. Getting data from the web

- Using `Biopython`
- Using `requests`
- Using `beautifulsoup`
- Using `scrapy`

**25** Testing, packaging and distribution

- Unit and functional tests
- Benchmarking
- Python: Packaging with `setuptools`
- Python: Publishing to PyPI
- Using [flit](https://flit.readthedocs.io/en/latest/)
- Deploying a Python `dash` application to the Cloud
