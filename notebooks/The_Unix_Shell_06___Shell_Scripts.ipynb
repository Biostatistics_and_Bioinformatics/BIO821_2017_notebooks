{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The Unix Shell: Writing Shell Scripts\n",
    "\n",
    "The shell commands constitute a programming language, and command line programs known as shell scripts can be written to perform complex tasks. \n",
    "\n",
    "This will only provide a brief overview - shell scripts have many traps and pitfalls for the unwary, and we generally prefer to use languages such as Python or R with more consistent syntax for complex tasks. However, shell scripts are extensively used in domains such as the preprocessing of genomics data, and it is a useful tool to know about."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## Assigning variables\n",
    "\n",
    "We assign variables using `=` and recall them by using `$`. It is customary to spell shell variable names in ALL_CAPS."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "NAME='Joe'\n",
    "echo \"Hello $NAME\"\n",
    "echo \"Hello ${NAME}\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Single and double parentheses\n",
    "\n",
    "The main difference between the use of '' and \"\" is that variable expansion only occurs with double parentheses. For plain text, they are equivalent."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "echo '${NAME}'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "echo \"${NAME}\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Use of parenthesis\n",
    "\n",
    "Use of parenthesis unambiguously specifies the variable of interest. I suggest you always use them as a defensive programming technique."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "echo \"Hello ${NAME}l\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$Namel is not defined, and so returns an empty string!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "echo \"Hello $NAMEl\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One of the quirks of shell scripts is already present - there cannot be spaces before or after the `=` in an assignment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "NAME2= 'Joe'\n",
    "echo \"Hello ${NAME2}\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "NAME3 ='Joe'\n",
    "echo \"Hello ${NAME3}\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Assigning commands to variables"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pwd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "CUR_DIR=$(pwd)\n",
    "dirname ${CUR_DIR}\n",
    "basename ${CUR_DIR}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Working with numbers\n",
    "\n",
    "**Careful**: Note the use of DOUBLE parentheses to trigger evaluation of a mathematical expression."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "NUM=$((1+2+3+4))\n",
    "echo ${NUM}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `seq` generates a range of numbers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "seq 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "seq 2 5"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "seq 5 2 9"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Attempt to find sum of first 5 numbers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "seq 5 | sum"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "whatis sum "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Doing this is surprisingly tricky\n",
    "\n",
    "Another reason to use Python or R where sum is just sum."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We first use command substitution to treat the output of `seq 5` as a file, then pass it to `paste` which inserts teh `+` delimiter between each line in the file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "whatis paste"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "paste -s -d+ <(seq 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This string is then passed to `bc`, which evaluates strings as mathematical expressions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "whatis bc"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "paste -s -d+ <(seq 5) | bc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Writing functions\n",
    "\n",
    "Functions in bash have this structure\n",
    "\n",
    "```bash\n",
    "function_name () {\n",
    "    commands\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Function arguments\n",
    "\n",
    "Function arguments are retrieved via special symbols known as positional parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "f () {\n",
    "    echo $0\n",
    "    echo $1\n",
    "    echo $2\n",
    "    echo $@\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f one two three"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Writing a sum function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "f_sum () {\n",
    "    paste -s -d+ $1 | bc\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f_sum <(seq 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Function to extract first line of a set of files"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ls *.txt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "headers () {\n",
    "    for FILE in $@; do\n",
    "        echo -n \"${FILE}:   \"\n",
    "        cat ${FILE} | head -n 1\n",
    "    done\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "headers $(ls *.txt)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Branching"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Using if to check for file existence"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if [ -f hello.txt ]; then\n",
    "    cat hello.txt\n",
    "else\n",
    "    echo \"No such file\"\n",
    "fi"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Downloading remote files"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "man wget | head -n 20"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wget -qO- https://vincentarelbundock.github.io/Rdatasets/doc/HSAUR/Forbes2000.html \\\n",
    "    | html2text | head -n 27  | tail -n 17"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "if [ ! -f \"data/forbes.csv\" ]; then\n",
    "    wget https://vincentarelbundock.github.io/Rdatasets/csv/HSAUR/Forbes2000.csv \\\n",
    "    -O data/forbes.csv\n",
    "fi"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Conditional evaluation with `test`\n",
    "\n",
    "The `[ -f hello.txt ]` syntax is equivalent to `test -f hello.txt`, where `test` is a shell command with a large range of operators and flags that you can view in the man page."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "man test | head -n 30"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Looping"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### For loop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for FILE in $(ls *txt); do\n",
    "    echo $FILE\n",
    "done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### While loop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "COUNTER=10\n",
    "while [ $COUNTER -gt 0 ]; do\n",
    "    echo $COUNTER\n",
    "    COUNTER=$(($COUNTER - 1))\n",
    "done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Careful**: Note that `<` is the redirection operator, and hence will lead to an infinite loop. Use `-lt` for less than and `-gt` for greater than,  `==` for equality and `!=` for inequality."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "COUNTER=10\n",
    "while [ $COUNTER != 0 ]; do\n",
    "    echo $COUNTER\n",
    "    COUNTER=$(($COUNTER - 1))\n",
    "done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Shell script"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From now on, we will write the shell script using an editor for convenience. For a syntax-highlighted display, I use a non-standard Python program `pygmentize` that you can install with \n",
    "\n",
    "```\n",
    "pip install pygments\n",
    "```\n",
    "\n",
    "but you can also just use `cat` to display the file contents.\n",
    "\n",
    "A shell script is traditionally given the extension `.sh`. There are a few things to note:\n",
    "\n",
    "1. To make the script standalone, you need to add `#!/path/to/shell` in the first line. Otherwise you need to call the script with `bash /path/to/script` instead of just `/path/to/script`.\n",
    "2. To make the script executable, change the file permissions to executable with `chmod +x /path/to/script`\n",
    "3. Shell arguments are similar to  function arguments - i.e. `$1`, `$2`, `$@` etc. Another useful variable is `$#` which gives the number of command line arguments."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Find default shell to use"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "which bash"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Display script"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pygmentize -g scripts/cat_if_exists.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Give executable permission"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "chmod +x scripts/cat_if_exists.sh"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scripts/cat_if_exists.sh hello.txt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scripts/cat_if_exists.sh goodbye.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Reading a file line by line\n",
    "\n",
    "We will write a script to extract headers from a FASTA Nucleic Acid (FNA) file. Headers in FASTA format are lines that begin with the `>` character."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat data/example.fna | head -n 23"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pygmentize scripts/extract_headers.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Careful**: You need to put all variables in the test condition within double quotes. If not, when the variable is empty or undefined (e.g. empty line) it vanishes and leaves `[ == '>' ]` which raises a syntax error."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "chmod +x scripts/extract_headers.sh"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat data/example.fna | scripts/extract_headers.sh"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
